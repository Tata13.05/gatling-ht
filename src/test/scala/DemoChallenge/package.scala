import io.gatling.core.Predef._
import io.gatling.http.Predef._

package object requests {

  val headers_0 = Map(
    "sec-ch-ua" -> """Opera";v="83", "Chromium";v="97", ";Not A Brand";v="99""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "document",
    "sec-fetch-mode" -> "navigate",
    "sec-fetch-site" -> "same-origin",
    "sec-fetch-user" -> "?1",
    "upgrade-insecure-requests" -> "1")

  val headers_1 = Map(
    "origin" -> "https://challenge.flood.io",
    "sec-ch-ua" -> """Opera";v="83", "Chromium";v="97", ";Not A Brand";v="99""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "document",
    "sec-fetch-mode" -> "navigate",
    "sec-fetch-site" -> "same-origin",
    "sec-fetch-user" -> "?1",
    "upgrade-insecure-requests" -> "1")

  val headers_5 = Map(
    "accept" -> "*/*",
    "sec-ch-ua" -> """Opera";v="83", "Chromium";v="97", ";Not A Brand";v="99""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "csors",
    "sec-fetch-site" -> "same-origin",
    "x-requested-with" -> "XMLHttpRequest")

  object HomePage {
    val openHomePage = exec(http("Open homepage")
      .get("/")
      .headers(headers_0)
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"")
        .find.saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(status.is(200))
    )
  }


  object StartTest {
    val clickStartTest = exec(http("Start test")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "1")
      .formParam("commit", "Start")
      .check(status.is(302))
    )

    val openStep2Page = exec(http("Open Step 2 page")
      .get("/step/2")
      .headers(headers_0)
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"")
        .find.saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(regex("<option value=\"(\\d+)\"").findRandom.saveAs("randomAge"))
      .check(status.is(200))
    )
  }

  object ChooseAge {
    val clickNextBotton = exec(http("Click Next Button")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "2")
      .formParam("challenger[age]", "${randomAge}")
      .formParam("commit", "Next")
      .check(status.is(302))
    )

    val openStep3Page = exec(http("Open step 3 page")
      .get("/step/3")
      .headers(headers_0)
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"").find
        .saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"")
        .find.saveAs("step_id"))
      .check(status.is(200))
      .check(css("label").findAll.transform(_.maxBy(s => s.toInt))
        .saveAs("max_number"))
      .check(css("label").findAll
        .saveAs("numbers"))
      .check(css("input[id^=challenger_order_selected]", "value").findAll
        .saveAs("order_selected_values")
      )
    ).exec(session => {
      val order_selected_values = session("order_selected_values").as[Vector[String]]
      val numbers = session("numbers").as[Vector[String]]
      val max_number = session("max_number").as[String]
      val map = (numbers zip order_selected_values).toMap
      session.set("value_for_max_number", map(max_number))
    })
  }

  object SelectBiggestValue {
    val selectBiggestValue = exec(http("Select biggest value")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "3")
      .formParam("challenger[largest_order]", "${max_number}")
      .formParam("challenger[order_selected]", "${value_for_max_number}")
      .formParam("commit", "Next")
      .check(status.is(302))
      .check(header("location").find.is("https://challenge.flood.io/step/4"))
    )

    val openStep4Page = exec(http("Open Step 4 page")
      .get("/step/4")
      .headers(headers_0)
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"")
        .find.saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(status.is(200))
      .check(css("input[id^=challenger_order]", "name").findAll.saveAs("challenger_order_names"))
      .check(css("input[id^=challenger_order]", "value").findAll.saveAs("challenger_order_values"))
    ).exec(session => {
      val challenger_order_names = session("challenger_order_names").as[Vector[String]]
      val challenger_order_values = session("challenger_order_values").as[Vector[String]]
      val map = (challenger_order_names zip challenger_order_values).toMap
      println(map)
      session.set("challenger_order_map", map)
    })
  }

  object TakeNextStep {

    val hitNextButton = exec(http("Hit Next Button")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "4")
      .formParamMap("${challenger_order_map}")
      .formParam("commit", "Next")
      .check(status.is(302))
      .check(header("location").find.is("https://challenge.flood.io/step/5"))
    )

    val step_5_OpenPage = exec(http("Open Step 5 page")
      .get("/step/5")
      .headers(headers_0)
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"")
        .find.saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(status.is(200))

      .resources(http("Get Code")
        .get("/code")
        .headers(headers_5)
        .check(status.is(200))
        .check(jsonPath("$.code").find.saveAs("code"))
      )
    )
  }

  object EnterToken {

    val enterToken = exec((http("Enter token"))
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "5")
      .formParam("challenger[one_time_token]", "${code}")
      .formParam("commit", "Next")
      .check(status.is(302))
      .check(header("location").find.is("https://challenge.flood.io/done"))
    )


    val goToDonePage = exec(http("Open Done page")
      .get("/done")
      .headers(headers_0)
      .check(status.is(200))
    )
  }

}
