package DemoChallenge

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import requests._

import scala.concurrent.duration.DurationInt

class RecordedSimulation extends Simulation {

  val th_min = 1
  val th_max = 2
  val test_duration = System.getProperty("duration", "30").toInt
  val test_users = System.getProperty("users", "1").toInt
  val test_ramp_up = System.getProperty("ramp_up", "5").toInt

  val httpProtocol = http
    .baseUrl("https://challenge.flood.io")
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""), WhiteList())
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 OPR/83.0.4254.54")
    .disableFollowRedirect

  val scn = scenario("RecordedSimulation")
    .during(test_duration.seconds, exitASAP = false) {
      exec(HomePage.openHomePage).pause(th_min, th_max)
        .exec(StartTest.clickStartTest).pause(th_min, th_max)
        .exec(StartTest.openStep2Page).pause(th_min, th_max)
        .exec(ChooseAge.clickNextBotton).pause(th_min, th_max)
        .exec(ChooseAge.openStep3Page).pause(th_min, th_max)
        .exec(SelectBiggestValue.selectBiggestValue).pause(th_min, th_max)
        .exec(SelectBiggestValue.openStep4Page).pause(th_min, th_max)
        .exec(TakeNextStep.hitNextButton).pause(th_min, th_max)
        .exec(TakeNextStep.step_5_OpenPage).pause(th_min, th_max)
        .exec(EnterToken.enterToken).pause(th_min, th_max)
        .exec(EnterToken.goToDonePage).pause(th_min, th_max)
    }

  setUp(scn.inject(
    rampConcurrentUsers(2).to(test_users).during(test_duration)
  ) protocols (httpProtocol))
    .assertions(
      global.responseTime.mean.lt(800),
      global.failedRequests.percent.lt(10))
}